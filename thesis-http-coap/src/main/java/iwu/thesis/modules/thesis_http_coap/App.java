package iwu.thesis.modules.thesis_http_coap;

/**
 * entry point
 *
 */
public class App {
	public static void main(String[] args) throws Exception {
		ServerHTTP serverHTTP = new ServerHTTP();
		serverHTTP.createServer();
	}
}
