package iwu.thesis.modules.thesis_http_coap;

import java.io.BufferedReader;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.AbstractHandler;

/**
 * ServerHTTP is Apache server implemented using "Jetty server". All the HTTP
 * request/response handles in this class. Methods like POST, GET, PUT, DELETE
 * handles here.
 * 
 * @author himmatbh
 *
 *
 */
public class ServerHTTP {

	private static final String CONFIG_FILE_LOCATION = "config/server_config.xml";

	/**
	 * Constructor of ServerHTTP class
	 */
	public ServerHTTP() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Method createServer() creates server instance and starts HTTP Server.
	 * 
	 * @throws Exception
	 */
	public void createServer() throws Exception {
		Server server = new Server(8686);
		ServerHTTP serverApache = new ServerHTTP();
		server.setHandler(serverApache.new ApacheServerHandler());
		server.start();
		server.join();
	}

	/**
	 * Class ApacheServerHandler extends AbstractHandler class of
	 * "jetty server". All the coming HTTP request/response traffic handles
	 * here.
	 * 
	 * @author himmatbh
	 *
	 */
	public class ApacheServerHandler extends AbstractHandler {

		@Override
		public void handle(String target,
				org.eclipse.jetty.server.Request baseRequest,
				HttpServletRequest request, HttpServletResponse response)
				throws IOException, ServletException {
			System.out.print("###### APACHE SERVER ######");
			System.out.println("HTTP Request Method : \t"
					+ baseRequest.getMethod());
			System.out.println("HTTP Request URL : \t"
					+ baseRequest.getRequestURL());
			System.out.println("HTTP Path : \t" + baseRequest.getPathInfo());

			// Read payload from the HTTP request
			BufferedReader reader = null;
			reader = request.getReader();
			StringBuilder out = new StringBuilder();
			String line;
			while ((line = reader.readLine()) != null) {
				out.append(line);
			}
			String data = out.toString();
			reader.close();
			System.out.println("HTTP Request Payload : \t" + data);
			Object responObject = null;
			XMLParserConfig xmlParserConfig = new XMLParserConfig();
			ServerNodeInfo nodeInfo = xmlParserConfig.parseResources(
					CONFIG_FILE_LOCATION, baseRequest.getPathInfo());
			ClientCoAP clientCoAP = new ClientCoAP();

			switch (request.getMethod()) {
			case "GET":
				responObject = clientCoAP.sendCoAPGET(nodeInfo.getAddress());
				break;
			case "POST":
				responObject = clientCoAP.sendCoAPPOST(nodeInfo.getAddress(),
						data);
				break;
			}

			response.setStatus(HttpServletResponse.SC_OK);

			baseRequest.setHandled(true);
			response.getWriter().println(responObject.toString());
		}
	}

}
