package iwu.thesis.modules.thesis_http_opcua;

import iwu.thesis.modules.thesis_http_opcua.opcua.ClientExampleRunner;
import iwu.thesis.modules.thesis_http_opcua.opcua.ReadExample;
import iwu.thesis.modules.thesis_http_opcua.opcua.WriteExample;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class EmbeddedAsyncServlet extends HttpServlet{

	private static final String CONFIG_FILE_LOCATION = "config/server_config.xml";
	
	 @Override
     protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException
     {
         final AsyncContext ctxt = req.startAsync();
         ctxt.start(new Runnable()
         {
             @Override
             public void run()
             {
                 System.err.println("In AsyncContext / Start / Runnable / run");
                 System.out.println(req.getPathInfo());
                 XMLParserConfig xmlParserConfig = new XMLParserConfig();
         		 ServerNodeInfo nodeInfo = xmlParserConfig.parseResources(
         				CONFIG_FILE_LOCATION, req.getPathInfo());
         		try {				
   				 new ClientExampleRunner(new ReadExample(nodeInfo.getPath(), ctxt)).run();				
   				
   			} catch (ExecutionException | InterruptedException e) {
   				// TODO Auto-generated catch block
   				e.printStackTrace();
   			}
               
             }
         });
     }
	 
	 @Override
	protected void doPost(final HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		 final AsyncContext ctxt = req.startAsync();
         ctxt.start(new Runnable()
         {
             @Override
             public void run()
             {
                 System.err.println("In AsyncContext / Start / Runnable / run");
                 System.out.println(req.getPathInfo());
              
                 XMLParserConfig x = new XMLParserConfig();
         		 ServerNodeInfo nodeInfo = x.parseResources(
         				"config/server_config.xml", req.getPathInfo());
         		
                 try {
                	// Read payload from the HTTP request
              		BufferedReader reader = null;
              		reader = ctxt.getRequest().getReader();
              		StringBuilder out = new StringBuilder();
              		String line;
              		while ((line = reader.readLine()) != null) {
              			out.append(line);
              		}
              		String data = out.toString();
              		reader.close();
              		
              		try {
        				if(isValueInteger(data))
        				{
        					int payload = Integer.parseInt(data);
        					new ClientExampleRunner(new WriteExample(nodeInfo.getPath(), payload, ctxt)).run();
        					ctxt.complete();
        				}
        				else
        				{        					
        					resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        					ctxt.complete();        					
        				}
        			} catch (ExecutionException | InterruptedException e) {
        				// TODO Auto-generated catch block
        				e.printStackTrace();
        			}
                	
					//ctxt.getResponse().getWriter().write("hello");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
               
             }
         });
	}
	 public static boolean isValueInteger(String s) {
		    try { 
		        Integer.parseInt(s); 
		    } catch(NumberFormatException e) { 
		        return false;//For example "bhushan" 
		    }
		    return true;// For example "21" 
		}
}
