package iwu.thesis.modules.thesis_http_opcua.opcua;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import javax.servlet.AsyncContext;

import org.eclipse.milo.opcua.sdk.client.OpcUaClient;
import org.eclipse.milo.opcua.stack.core.types.builtin.DataValue;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;
import org.eclipse.milo.opcua.stack.core.types.builtin.StatusCode;
import org.eclipse.milo.opcua.stack.core.types.builtin.Variant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableList;

public class WriteExample implements ClientExample {

	String nodePathString;
	AsyncContext httpContext;
	Object writeValue;

	/*
	 * public static void main(String[] args) throws Exception { WriteExample
	 * example = new WriteExample();
	 * 
	 * new ClientExampleRunner(example).run(); }
	 */

	public WriteExample(String nodePath, Object value, AsyncContext httpContext)
			throws ExecutionException, InterruptedException {
		// TODO Auto-generated constructor stub

		this.nodePathString = nodePath;
		this.writeValue = value;
		this.httpContext = httpContext;
		// this.example = new ReadExample();
		// new ClientExampleRunner(this.example).run();
	}

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public void run(OpcUaClient client, CompletableFuture<OpcUaClient> future)
			throws Exception {
		// synchronous connect
		client.connect().get();

		List<NodeId> nodeIds = ImmutableList.of(new NodeId(2, nodePathString));

		// for (int i = 0; i < 10; i++) {
		Variant v = new Variant(writeValue);

		// don't write status or timestamps
		DataValue dv = new DataValue(v, null, null);

		// write asynchronously....
		CompletableFuture<List<StatusCode>> f = client.writeValues(nodeIds,
				ImmutableList.of(dv));

		// ...but block for the results so we write in order
		List<StatusCode> statusCodes = f.get();
		StatusCode status = statusCodes.get(0);
		this.httpContext.complete();
		if (status.isGood()) {
			logger.info("Wrote '{}' to nodeId={}", v, nodeIds.get(0));

		}
		// }

		future.complete(client);
	}

}