package iwu.thesis.modules.thesis_http_mqtt;

import javax.servlet.AsyncContext;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttTopic;

public class ClientMQTT {
	private String BROKER_URL = null;
	private String TOPIC = null;
	// public static final String BROKER_URL =
	// "tcp://linkedfactory.iwu.fraunhofer.de:8883";
	public static final String CLIENT_ID = "Isjfhcerefdoradiys";
	private static MqttClient client;
	private static MqttClient clientSubscribe;

	public ClientMQTT(String broker, String topic) {
		this.BROKER_URL = broker;
		this.TOPIC = topic;
		// String clientId = MqttClient.generateClientId() + "-pub";
		// String clientId = CLIENT_ID + "-pub";

		try {
			client = new MqttClient(BROKER_URL, MqttClient.generateClientId());
			MqttConnectOptions options = new MqttConnectOptions();
			options.setCleanSession(false);
			options.setWill(client.getTopic(TOPIC + "/LWT"),
					"I'm gone".getBytes(), 2, true);
			if (!client.isConnected())
				client.connect(options);

		} catch (MqttException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	public void publishTopic(String topic, String data, final AsyncContext httpContext) throws MqttException {

		final MqttTopic mqttTopic = client.getTopic(topic);

		// final int temperatureNumber = getRandomNumber(20, 30);
		// final String temperature = temperatureNumber + "°C";
		MqttMessage message = new MqttMessage(data.getBytes());
		message.setQos(2);
		message.setRetained(true);
		mqttTopic.publish(message);
		httpContext.complete();
		System.err.println("topic published publishTopic() :" + topic + " : "
				+ data);
		client.disconnect();
	}

	public  synchronized void subscribeTopic(String topic, final AsyncContext httpContext) throws MqttException,
			InterruptedException {
		try {
			System.out.println("MQTT : Topic: \t " + topic + " \n");
			// MqttClient c = new MqttClient("tcp://mqtt-dashboard.com:1883",
			// MqttClient.generateClientId());
			clientSubscribe = new MqttClient(BROKER_URL,
					MqttClient.generateClientId());
			HelloCallback hb = new HelloCallback();

			clientSubscribe.setCallback(new MqttCallback() {

				public void messageArrived(String subTopic, MqttMessage message)
						throws Exception {
					// TODO Auto-generated method stub
					System.out
							.println("Message arrived : subscribeTopic(). Topic: "
									+ subTopic
									+ " Message: "
									+ message.toString());
					
					httpContext.getResponse().getWriter().write(message.toString());
					httpContext.complete();
					
					if ((TOPIC + "/LWT").equals(subTopic)) {
						System.err.println("Sensor gone!");
						//ov.setValue("Sensor gone!");
					}
				}

				public void deliveryComplete(IMqttDeliveryToken arg0) {
					// TODO Auto-generated method stub
				}

				public void connectionLost(Throwable arg0) {
					// TODO Auto-generated method stub
				}
			});

			clientSubscribe.connect();
			clientSubscribe.subscribe(topic);

			// c.disconnect();
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	class HelloCallback implements MqttCallback {

		String dataMessage = null;
		boolean gotAnswer = false;

		public HelloCallback() {
			// TODO Auto-generated constructor stub

		}

		public void messageArrived(String arg0, MqttMessage message)
				throws Exception {

			// TODO Auto-generated method stub
			System.out.println("Message arrived : subscribeTopic(). Topic: "
					+ arg0 + " Message: " + message.toString());

			if ((TOPIC + "/LWT").equals(arg0)) {
				System.err.println("Sensor gone!");
				dataMessage = "sensor gone!";
			}

		}

		public void deliveryComplete(IMqttDeliveryToken arg0) {
			// TODO Auto-generated method stub
		}

		public void connectionLost(Throwable arg0) {
			// TODO Auto-generated method stub

		}

	}
}
