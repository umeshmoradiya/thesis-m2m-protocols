package iwu.thesis.modules.thesis_http_mqtt;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.json.JSONObject;
import org.json.XML;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XMLParserConfig {

	String[] valueArray = { "ob", "R", "W", "RR" };

	public XMLParserConfig() {
		// TODO Auto-generated constructor stub
	}

	public ServerNodeInfo parseResources(String filePath, String expression) {

		try {
			XMLParserConfig obj = new XMLParserConfig();
			File inputFile = obj.getFile(filePath);

			String expressionStr = expression;
			String[] ary = expressionStr.split("/");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

			Document doc = dBuilder.parse(inputFile);
			NodeList root = doc.getChildNodes();
			Node finalNode = null;
			for (int i = 0; i < ary.length; i++) {

				if (!ary[i].isEmpty()) {
					if (Arrays.asList(valueArray).contains(ary[i])) {
						continue;
					}else if(obj.getNode("node", "name", ary[i], finalNode!= null ? finalNode.getChildNodes() : root) != null){
						finalNode = obj.getNode("node", "name", ary[i],
								finalNode != null ? finalNode.getChildNodes(): root);
					}

				}
			}

			JSONObject jsonObject = XML
					.toJSONObject(convertNodeToHtml(finalNode));
			String jsonPrettyPrintString = jsonObject.toString(4);
			System.out.println(jsonPrettyPrintString);
			ServerNodeInfo srvNodeInfo = new ServerNodeInfo();
			srvNodeInfo.setAddress(jsonObject.getJSONObject("node")
					.getString("address"));
			srvNodeInfo.setProtocol(jsonObject.getJSONObject("node")
					.getString("protocol"));
			srvNodeInfo.setName(jsonObject.getJSONObject("node").getString(
					"name"));
			srvNodeInfo.setId(jsonObject.getJSONObject("node").getInt("id"));
			srvNodeInfo.setPath(jsonObject.getJSONObject("node").getString(
					"nodeuri"));

			return srvNodeInfo;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	protected Node getNode(String tagName, NodeList nodes) {
		for (int x = 0; x < nodes.getLength(); x++) {
			Node node = nodes.item(x);
			if (node.getNodeName().equalsIgnoreCase(tagName)) {
				return node;
			}
		}
		return null;
	}

	protected Node getNode(String tagName, String attrName, String attrValue,
			NodeList nodes) {
		for (int x = 0; x < nodes.getLength(); x++) {
			Node node = nodes.item(x);
			if (node.hasAttributes()) {
				// System.out.println("\t" + node.getNodeName());
				NamedNodeMap attrs = node.getAttributes();
				// System.out.println("\t" + attrs.getLength());
				for (int y = 0; y < attrs.getLength(); y++) {
					Node attr = attrs.item(y);
					if (attr.getNodeName().equalsIgnoreCase(attrName)
							&& attr.getNodeValue().equals(attrValue)) {
						return node;
					}
				}
			}

		}
		return null;
	}

	public String getNodeValue(Node node) {
		NodeList childNodes = node.getChildNodes();
		for (int x = 0; x < childNodes.getLength(); x++) {
			Node data = childNodes.item(x);
			if (data.getNodeType() == Node.TEXT_NODE)
				return data.getNodeValue();
		}
		return "";
	}

	// http://tech.chitgoks.com/2010/05/06/convert-org-w3c-dom-node-to-string-using-java/
	public static String convertNodeToHtml(Node node)
			throws TransformerException {
		Transformer t = TransformerFactory.newInstance().newTransformer();
		t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		StringWriter sw = new StringWriter();
		t.transform(new DOMSource(node), new StreamResult(sw));
		return sw.toString();
	}

	public String getNodeValue(String tagName, NodeList nodes) {
		for (int x = 0; x < nodes.getLength(); x++) {
			Node node = nodes.item(x);
			if (node.getNodeName().equalsIgnoreCase(tagName)) {
				NodeList childNodes = node.getChildNodes();
				for (int y = 0; y < childNodes.getLength(); y++) {
					Node data = childNodes.item(y);
					if (data.getNodeType() == Node.TEXT_NODE)
						return data.getNodeValue();
				}
			}
		}
		return "";
	}

	public String getNodeAttr(String attrName, String attrValue, Node node) {
		NamedNodeMap attrs = node.getAttributes();
		for (int y = 0; y < attrs.getLength(); y++) {
			Node attr = attrs.item(y);
			if (attr.getNodeName().equalsIgnoreCase(attrName)
					&& attr.getNodeValue().equals(attrValue)) {
				return attr.getNodeValue();
			}
		}
		return "";
	}

	// http://www.drdobbs.com/jvm/creating-and-modifying-xml-in-java/240150782
	public String getNodeAttr(String tagName, String attrName,
			String attrValue, NodeList nodes) {
		for (int x = 0; x < nodes.getLength(); x++) {
			Node node = nodes.item(x);
			if (node.getNodeName().equalsIgnoreCase(tagName)) {
				NodeList childNodes = node.getChildNodes();
				for (int y = 0; y < childNodes.getLength(); y++) {
					Node data = childNodes.item(y);

					if (data.getNodeType() == Node.ATTRIBUTE_NODE) {
						if (data.getNodeName().equalsIgnoreCase(attrName)
								&& data.getNodeValue().equalsIgnoreCase(
										attrValue)) {
							System.out.println(attrValue);
							return data.getNodeValue();
						}
					}

				}
			}
		}

		return "";
	}

	public File getFile(String fileName) {

		StringBuilder result = new StringBuilder("");

		// Get file from resources folder
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource(fileName).getFile());

		try (Scanner scanner = new Scanner(file)) {

			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				result.append(line).append("\n");
			}

			scanner.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

		return file;

	}
}
