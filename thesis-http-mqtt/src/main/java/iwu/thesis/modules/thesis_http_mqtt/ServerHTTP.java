package iwu.thesis.modules.thesis_http_mqtt;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

/**
 * ServerHTTP is Apache server implemented using "Jetty server". All the HTTP
 * request/response handles in this class. Methods like POST, GET, PUT, DELETE
 * handles here.
 * 
 * @author himmatbh
 *
 *
 */
public class ServerHTTP {
	
	/**
	 * Constructor of ServerHTTP class
	 */
	public ServerHTTP() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * Method createServer() creates server instance and starts HTTP Server.
	 * 
	 * @throws Exception
	 */
	public void createServer() throws Exception {
		Server server = new Server(8686);
		//server.setHandler(new JettyServerHandler());
		
		 ServletContextHandler context = new ServletContextHandler();
	        context.setContextPath("/");
	        ServletHolder asyncHolder = context.addServlet(EmbeddedAsyncServlet.class,"/*");
	        asyncHolder.setAsyncSupported(true);
	        server.setHandler(context);
		
		server.start();
		server.join();
	}
}
