package iwu.thesis.modules.thesis_http_mqtt;

import java.io.BufferedReader;
import java.io.IOException;

import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.paho.client.mqttv3.MqttException;

public class EmbeddedAsyncServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final String CONFIG_FILE_LOCATION = "config/server_config.xml";
	private static final String PREFIX_OBSERVE = "/O";
	private static final String PREFIX_GET = "/R";
	private static final String PREFIX_POST = "/W";

	@Override
	protected void doGet(final HttpServletRequest req,
			final HttpServletResponse resp) throws ServletException,
			IOException {
		final AsyncContext ctxt = req.startAsync();
		ctxt.start(new Runnable() {
			@Override
			public void run() {
				System.err.println("In AsyncContext / Start / Runnable / run");
				System.out.println(req.getPathInfo());
				XMLParserConfig xmlParserConfig = new XMLParserConfig();
				ServerNodeInfo nodeInfo = xmlParserConfig.parseResources(
						CONFIG_FILE_LOCATION, req.getPathInfo());
				ClientMQTT clientMQTT = new ClientMQTT(nodeInfo.getAddress(),
						PREFIX_GET + nodeInfo.getPath());
				try {
					clientMQTT.subscribeTopic(PREFIX_GET + nodeInfo.getPath(), ctxt);
					// ctxt.getResponse().getWriter().write("hello");
				} catch (MqttException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});
	}

	@Override
	protected void doPost(final HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		final AsyncContext ctxt = req.startAsync();
		ctxt.start(new Runnable() {
			@Override
			public void run() {
				System.err.println("In AsyncContext / Start / Runnable / run");
				System.out.println(req.getPathInfo());

				XMLParserConfig xmlParserConfig = new XMLParserConfig();
				ServerNodeInfo nodeInfo = xmlParserConfig.parseResources(
						CONFIG_FILE_LOCATION, req.getPathInfo());
				ClientMQTT clientMQTT = new ClientMQTT(nodeInfo.getAddress(),
						PREFIX_POST + nodeInfo.getPath());
				try {
					// Read payload from the HTTP request
					BufferedReader reader = null;
					reader = ctxt.getRequest().getReader();
					StringBuilder out = new StringBuilder();
					String line;
					while ((line = reader.readLine()) != null) {
						out.append(line);
					}
					String data = out.toString();
					reader.close();

					clientMQTT.publishTopic(PREFIX_POST + nodeInfo.getPath(), data,
							ctxt);
					// ctxt.getResponse().getWriter().write("hello");
				} catch (MqttException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});
	}
}
