package iwu.thesis.modules.thesis_opcua_mqtt;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttTopic;

public class ClientMQTT {
	private String BROKER_URL = null;
	private String TOPIC = null;
	// public static final String BROKER_URL =
	// "tcp://linkedfactory.iwu.fraunhofer.de:8883";
	public static final String CLIENT_ID = "Isjfhcerefdoradiys";
	private static MqttClient client;
	private static MqttClient clientSubscribe;

	public ClientMQTT(String broker, String topic) {
		this.BROKER_URL = broker;
		this.TOPIC = topic;
		// String clientId = MqttClient.generateClientId() + "-pub";
		// String clientId = CLIENT_ID + "-pub";

		try {
			client = new MqttClient(BROKER_URL, MqttClient.generateClientId());
			MqttConnectOptions options = new MqttConnectOptions();
			options.setCleanSession(false);
			options.setWill(client.getTopic(TOPIC + "/LWT"),
					"I'm gone".getBytes(), 2, true);
			if (!client.isConnected())
				client.connect(options);

		} catch (MqttException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	public void publishTopic(String topic, Object data) throws MqttException {

		final MqttTopic mqttTopic = client.getTopic(topic);

		// final int temperatureNumber = getRandomNumber(20, 30);
		// final String temperature = temperatureNumber + "°C";
		MqttMessage message = new MqttMessage(data.toString().getBytes());
		message.setQos(2);
		message.setRetained(true);
		mqttTopic.publish(message);

		System.err.println("topic published publishTopic() :" + topic + " : "
				+ data);
		client.disconnect();
	}

	public void subscribeTopic(String topic) throws MqttException,
			InterruptedException {
		try {
			System.out.println("MQTT : Topic: \t " + topic + " \n");
			// MqttClient c = new MqttClient("tcp://mqtt-dashboard.com:1883",
			// MqttClient.generateClientId());
			clientSubscribe = new MqttClient(BROKER_URL,
					MqttClient.generateClientId());
			HelloCallback hb = new HelloCallback();

			clientSubscribe.setCallback(hb);
			/*
			 * clientSubscribe.setCallback(new MqttCallback() {
			 * 
			 * public void messageArrived(String subTopic, MqttMessage message)
			 * throws Exception { // TODO Auto-generated method stub System.out
			 * .println("Message arrived : subscribeTopic(). Topic: " + subTopic
			 * + " Message: " + message.toString()); IObservable ov = new
			 * IObservable(message.toString()); // ov.addObserver(to);
			 * ov.setValue(message.toString());
			 * 
			 * if ((TOPIC + "/LWT").equals(subTopic)) {
			 * System.err.println("Sensor gone!"); } }
			 * 
			 * public void deliveryComplete(IMqttDeliveryToken arg0) { // TODO
			 * Auto-generated method stub }
			 * 
			 * public void connectionLost(Throwable arg0) { // TODO
			 * Auto-generated method stub } });
			 */

			clientSubscribe.connect();
			clientSubscribe.subscribe(topic);
			synchronized (hb) {
				while (!hb.gotAnswer)
					// while instead of if due to "spurious wakeups"
					hb.wait();
			}
			System.out.println("Message from Callback: " + hb.dataMessage);
			// c.disconnect();
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	class HelloCallback implements MqttCallback {

		String dataMessage = null;
		boolean gotAnswer = false;

		public synchronized void messageArrived(String arg0, MqttMessage message)
				throws Exception {

			// TODO Auto-generated method stub
			System.out.println("Message arrived : subscribeTopic(). Topic: "
					+ arg0 + " Message: " + message.toString());
			gotAnswer = true;
			dataMessage = message.toString();
			if ((TOPIC + "/LWT").equals(arg0)) {
				System.err.println("Sensor gone!");
			}
			notify();

		}

		public synchronized void deliveryComplete(IMqttDeliveryToken arg0) {
			// TODO Auto-generated method stub
		}

		public synchronized void connectionLost(Throwable arg0) {
			// TODO Auto-generated method stub
			System.out.println("Message connection Lost");
		}

	}

}
