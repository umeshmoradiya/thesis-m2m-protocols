package iwu.thesis.modules.thesis_opcua_mqtt;

import java.util.Observable;

public class IObservable extends Observable {
	private String n = "obs";

	public IObservable(String n) {
		this.n = n;
	}

	public void setValue(String n) {
		this.n = n;
		setChanged();
		notifyObservers();
	}

	public String getValue() {
		return n;
	}

}
