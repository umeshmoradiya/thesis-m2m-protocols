package iwu.thesis.centralclient.thesis_coap_server;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.coap.CoAP.Type;
import org.eclipse.californium.core.server.resources.CoapExchange;

public class ROLLEXResource extends CoapResource {

	String buffer = "hello world";

	public ROLLEXResource() {

		// set resource identifier
		super("solarplant");
		setObservable(true);
		setObserveType(Type.CON);
		getAttributes().setTitle("Powermeter Resource");

	}

	@Override
	public void handleGET(CoapExchange exchange) {
		System.out.println("ROLLEXResource handleCoAPGET() : " + buffer);

		exchange.respond(buffer);

	}

	@Override
	public void handlePOST(CoapExchange exchange) {
		// TODO Auto-generated method stub
		System.out.println("ROLLEXResource handlePOST() : "
				+ exchange.getRequestText());
		buffer = exchange.getRequestText();
		exchange.respond(exchange.getRequestText());
		changed();
	}

}
