package iwu.thesis.centralclient.thesis_coap_server;

/**
 * Hello world!
 *
 */

import java.io.IOException;
import java.net.BindException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

import org.eclipse.californium.core.CoapServer;
import org.eclipse.californium.core.network.CoapEndpoint;
import org.eclipse.californium.core.network.EndpointManager;
import org.eclipse.californium.proxy.DirectProxyCoapResolver;
import org.eclipse.californium.proxy.ProxyHttpServer;
import org.eclipse.californium.proxy.resources.ForwardingResource;
import org.eclipse.californium.proxy.resources.ProxyCoapClientResource;
import org.eclipse.californium.proxy.resources.ProxyHttpClientResource;

public class AppCoAPServer extends CoapServer {
	private static final int COAP_PORT = 5694;

	public static void main(String[] args) {

		try {

			// create server
			AppCoAPServer server = new AppCoAPServer();
			// add endpoints on all IP addresses
			server.addEndpoints();

			// create Proxy setup and resource forwarding

			ForwardingResource coap2coap = new ProxyCoapClientResource(
					"coap2coap");
			ForwardingResource coap2http = new ProxyHttpClientResource(
					"coap2http");
			server.add(coap2coap);
			server.add(coap2http);

			server.start();

			ProxyHttpServer httpServer = new ProxyHttpServer(9595);
			httpServer.setProxyCoapResolver(new DirectProxyCoapResolver(
					coap2coap));

			System.out
					.println("CoAP resource \"target\" available over HTTP at: http://localhost:8080/proxy/coap://localhost:PORT/target");

		} catch (BindException e) {
			System.err.println("Already running.");
			System.exit(1);
		} catch (SocketException e) {
			System.err
					.println("Failed to initialize server: " + e.getMessage());
		} catch (IOException e) {
			System.err.println("Unexpected error.");
			e.printStackTrace();
			System.exit(2);
		}

	}

	/**
	 * Add individual endpoints listening on default CoAP port on all IPv4
	 * addresses of all network interfaces.
	 * 
	 * @throws UnknownHostException
	 */
	private void addEndpoints() throws UnknownHostException {
		for (InetAddress addr : EndpointManager.getEndpointManager()
				.getNetworkInterfaces()) {
			// only binds to IPv4 addresses and localhost
			if (addr instanceof Inet4Address || addr.isLoopbackAddress()) {
				InetAddress stAddr = InetAddress.getByName("10.102.201.93");
				InetSocketAddress bindToAddress = new InetSocketAddress(addr,
						COAP_PORT);
				addEndpoint(new CoapEndpoint(bindToAddress));
			}
		}
	}

	/**
	 * Constructor for a new server. Here, the resources of the server are
	 * initialized.
	 */
	public AppCoAPServer() {

		// provide an instance of a resource
		// add(new CoAPResource(mq));
		add(new ROLLEXResource());

		// add(new HelloWorldResource());
	}

}
