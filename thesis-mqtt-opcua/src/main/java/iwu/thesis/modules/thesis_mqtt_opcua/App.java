package iwu.thesis.modules.thesis_mqtt_opcua;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) throws Exception {
		App appServer = new App();
		appServer.subscribeMQTTClient("/R/iwu/fofab/nshv");
		
	}

	protected void subscribeMQTTClient(String topic) throws Exception {
		ClientMQTT clientMQTT = new ClientMQTT(topic);
		clientMQTT.subscribeTopic(topic);
	}
}
