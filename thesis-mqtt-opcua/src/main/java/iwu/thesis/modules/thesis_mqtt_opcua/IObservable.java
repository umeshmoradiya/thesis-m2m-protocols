package iwu.thesis.modules.thesis_mqtt_opcua;

import java.util.Observable;

public class IObservable extends Observable {
	private Object n = "obs";
	private String topic = "obs";

	public IObservable(Object n) {
		this.n = n;
	}

	public void setValue(Object n) {
		this.n = n;
		setChanged();
		notifyObservers();
	}

	public Object getValue() {
		return n;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

}
