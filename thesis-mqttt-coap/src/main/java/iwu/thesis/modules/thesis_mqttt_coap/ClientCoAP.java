package iwu.thesis.modules.thesis_mqttt_coap;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapHandler;
import org.eclipse.californium.core.CoapObserveRelation;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.core.Utils;

public class ClientCoAP {

	public ClientCoAP() {
		// TODO Auto-generated constructor stub
	}

	public Object sendCoAPGET(String url) {

		URI uri = null; // URI parameter of the request

		// input URI from command line arguments
		try {
			uri = new URI(url);

			CoapClient client = new CoapClient(uri);

			CoapResponse response = client.get();

			if (response != null) {

				System.out.println(response.getCode());
				System.out.println(response.getOptions());
				System.out.println(response.getResponseText());

				System.out.println("\nADVANCED\n");
				// access advanced API with access to more details through
				// .advanced()
				System.out.println(Utils.prettyPrint(response));
				return response.getResponseText();
			} else {
				System.out.println("No response received.");
			}
		} catch (URISyntaxException e) {
			System.err.println("Invalid URI: " + e.getMessage());
		}
		return null;
	}

	public Object sendCoAPPOST(String url, String data) {
		URI uri = null; // URI parameter of the request

		// input URI from command line arguments
		try {
			System.out.print("###### CoAP SERVER-CLIENT ######");
			uri = new URI(url);

			CoapClient client = new CoapClient(uri);

			CoapResponse response = client.post(data.toString().getBytes(), 2);

			if (response != null) {

				System.out.println(response.getCode());
				System.out.println(response.getOptions());
				System.out.println(response.getResponseText());

				System.out.println("\nADVANCED\n");
				// access advanced API with access to more details through
				// .advanced()
				System.out.println(Utils.prettyPrint(response));
				return response;
			} else {
				System.out.println("No response received.");
			}

			System.out.print("###### CoAP SERVER-CLIENT ENDS ######");
		} catch (URISyntaxException e) {
			System.err.println("Invalid URI: " + e.getMessage());
			System.exit(-1);
		}
		return null;
	}

	public Object sendCoAPObserve(String url, final String topic) {

		URI uri = null; // URI parameter of the request

		// input URI from command line arguments
		try {
			uri = new URI(url);
			System.out.print("###### CoAP SERVER-CLIENT-OBSERVE ######");
			CoapClient client = new CoapClient(uri);
			// wait for user

			CoapObserveRelation relation = client.observe(new CoapHandler() {

				@Override
				public void onLoad(CoapResponse response) {
					String contents = response.getResponseText();
					System.out.println("NOTIFICATION: " + response.toString());
					System.out.println(Utils.prettyPrint(response));

					IObservable ov = new IObservable(contents);
					ov.setTopic(topic);
					ClientMQTT to = new ClientMQTT(ov);
					ov.addObserver(to);
					ov.setValue(contents);

				}

				@Override
				public void onError() {
					System.err
							.println("OBSERVING FAILED (press enter to exit)");
				}

			});
			BufferedReader br = new BufferedReader(new InputStreamReader(
					System.in));
			try {
				br.readLine();
			} catch (IOException e) {
			}
			// wait for user
			try {
				br.readLine();
			} catch (IOException e) {
			}

			System.out.println("CANCELLATION");

			relation.proactiveCancel();
			return "";

		} catch (URISyntaxException e) {
			System.err.println("Invalid URI: " + e.getMessage());
			System.exit(-1);
		}
		return null;
	}

}
