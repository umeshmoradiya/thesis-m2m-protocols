package iwu.thesis.modules.thesis_mqttt_coap;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) throws Exception {
		App appServer = new App();
		appServer.subscribeMQTTClient("/O/iwu/fofab/solarplant");
		
	}

	protected void subscribeMQTTClient(String topic) throws Exception {
		ClientMQTT clientMQTT = new ClientMQTT(topic);
		clientMQTT.subscribeTopic(topic);
	}
}
