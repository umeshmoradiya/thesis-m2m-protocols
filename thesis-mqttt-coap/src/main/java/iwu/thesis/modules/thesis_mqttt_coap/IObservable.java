package iwu.thesis.modules.thesis_mqttt_coap;

import java.util.Observable;

public class IObservable extends Observable {
	private String n = "obs";
	private String topic = "obs";

	public IObservable(String n) {
		this.n = n;
	}

	public void setValue(String n) {
		this.n = n;
		setChanged();
		notifyObservers();
	}

	public String getValue() {
		return n;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

}
