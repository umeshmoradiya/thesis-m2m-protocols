package iwu.thesis.modules.thesis_mqttt_coap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttTopic;

public class ClientMQTT implements Observer {
	private static final String CONFIG_FILE_LOCATION = "config/server_config.xml";
	private String BROKER_URL = null;
	private String TOPIC = null;
	// public static final String BROKER_URL =
	// "tcp://linkedfactory.iwu.fraunhofer.de:8883";
	public static final String CLIENT_ID = "Isjfhcerefdoradiys";
	private static MqttClient client;
	private static MqttClient clientSubscribe;

	public ClientMQTT(String topic) {
		this.BROKER_URL = "tcp://broker.hivemq.com:1883";
		this.TOPIC = topic;
		// String clientId = MqttClient.generateClientId() + "-pub";
		// String clientId = CLIENT_ID + "-pub";

		try {
			client = new MqttClient(BROKER_URL, MqttClient.generateClientId());
			MqttConnectOptions options = new MqttConnectOptions();
			options.setCleanSession(false);
			options.setWill(client.getTopic(TOPIC + "/LWT"),
					"I'm gone".getBytes(), 2, true);
			if (!client.isConnected())
				client.connect(options);

		} catch (MqttException e) {
			e.printStackTrace();

		}
	}

	private IObservable ov = null;

	public ClientMQTT(IObservable ov) {
		this.ov = ov;
	}

	public void publishTopic(String topic, String data) throws MqttException {

		final MqttTopic mqttTopic = client.getTopic(topic);

		// final int temperatureNumber = getRandomNumber(20, 30);
		// final String temperature = temperatureNumber + "°C";
		MqttMessage message = new MqttMessage(data.getBytes());
		message.setQos(2);
		message.setRetained(true);
		mqttTopic.publish(message);

		System.err.println("topic published publishTopic() :" + topic + " : "
				+ data);
	}

	public void subscribeTopic(String topic) throws MqttException {
		try {
			System.out.println("MQTT : \n");

			clientSubscribe = new MqttClient(BROKER_URL,
					MqttClient.generateClientId());

			clientSubscribe.setCallback(new MqttCallback() {

				public void messageArrived(String subTopic, MqttMessage message)
						throws Exception {
					// TODO Auto-generated method stub
					System.out
							.println("Message arrived : subscribeTopic(). Topic: "
									+ subTopic
									+ " Message: "
									+ message.toString());

					XMLParserConfig xmlParserConfig = new XMLParserConfig();
					ServerNodeInfo nodeInfo = xmlParserConfig.parseResources(
							CONFIG_FILE_LOCATION, subTopic);

					ClientCoAP clientCoAP = new ClientCoAP();
					clientCoAP.sendCoAPObserve(nodeInfo.getAddress(), subTopic);

					if ((TOPIC + "/LWT").equals(subTopic)) {
						System.err.println("Sensor gone!");
					}
				}

				public void deliveryComplete(IMqttDeliveryToken arg0) {
					// TODO Auto-generated method stub
				}

				public void connectionLost(Throwable arg0) {
					// TODO Auto-generated method stub
				}
			});
			clientSubscribe.connect();
			clientSubscribe.subscribe(topic);
			// c.disconnect();
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		if (o == ov) {
			System.out.println("MQTT Client Observer : " + ov.getValue());
			System.out.println("MQTT Client Observer Topic : " + ov.getTopic());
			String topic = ov.getTopic();
			String[] array = topic.split("/");
			List<String> asSet = new ArrayList<String>(Arrays.asList(array));
			asSet.remove("O");
			array = asSet.toArray(new String[] {});
			System.out.println(Arrays.toString(array));
			System.out.println(getUriPathString(array));
			try {
				publishTopic("/E" + getUriPathString(array), ov.getValue());
			} catch (MqttException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public String getUriPathString(String[] array) {
		StringBuilder buffer = new StringBuilder();
		for (String element : array)
			buffer.append(element).append("/");
		if (buffer.length() == 0)
			return "";
		else
			return buffer.substring(0, buffer.length() - 1);
	}
}
