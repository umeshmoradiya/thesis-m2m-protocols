package iwu.thesis.modules.thesis_mqttt_coap;

public class Utils {

	public ServerNodeInfo parseResourcePath(String pathString) {
		XMLParserConfig x = new XMLParserConfig();
		ServerNodeInfo nodeInfo = x.parseResources("config/server_config.xml",
				pathString);
		System.out.print("\n###### Configuration File Parsing Start ######\n");
		System.out.println("Input Path : " + pathString);
		System.out
				.println("Translation Protocol   : " + nodeInfo.getProtocol());
		System.out.println("Translation URL : " + nodeInfo.getAddress());
		System.out.print("\n###### Configuration File Parsing End ######\n");

		return nodeInfo;
	}

}
