README.txt file for Multi-protocol communication management system

Project Modules:

1) CoAP to MQTT 	: thesis-coap-mqtt
	Main file	: CoapAppServer.java

2) CoAP to OPCUA	: thesis-coap-opcua
	Main file	: CoapAppServer.java

3) HTTP to CoAP		: thesis-http-coap
	Main file	: App.java

4) HTTP to MQTT		: thesis-http-mqtt
	Main file	: App.java

5) HTTP to OPCUA	: thesis-http-opcua
	Main file	: App.java

6) MQTT to OPCUA	: thesis-mqtt-opcua
	Main file	: App.java

7) MQTT to CoAP		: thesis-mqttt-coap
	Main file	: App.java

8) OPCUA to CoAP	: thesis-opcua-coap
	Main file	: ServerOPC.java

9) OPCUA to MQTT	: thesis-opcua-mqtt
	Main file	: ServerOPC.java


Testing Modules: 

1) CoAP server		: thesis-coap-server
	Main file	: AppCoAPServer.java

2) CoAP client		: Mozzila Firefox Plugin : Callifornium

3) OPC UA server	: thesis-module-opcua-server
	Main file	: ServerOPC.java

4) OPC UA client	: thesis-module-opcua-client
	Main file	: Read  : ReadExample.java
			: Write : WriteExample.java
5) MQTT broker		: tcp://broker.hivemq.com 1883

6) HTTP client		: Chrome Plugin : Advanced REST Client Application
