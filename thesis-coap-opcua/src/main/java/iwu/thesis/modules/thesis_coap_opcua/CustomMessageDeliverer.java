package iwu.thesis.modules.thesis_coap_opcua;

import iwu.thesis.modules.thesis_coap_opcua.opcua.ClientExampleRunner;
import iwu.thesis.modules.thesis_coap_opcua.opcua.ReadExample;
import iwu.thesis.modules.thesis_coap_opcua.opcua.WriteExample;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.ExecutionException;

import org.eclipse.californium.core.CoapServer;
import org.eclipse.californium.core.coap.CoAP.Code;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.coap.Request;
import org.eclipse.californium.core.coap.Response;
import org.eclipse.californium.core.network.Exchange;
import org.eclipse.californium.core.network.config.NetworkConfig;
import org.eclipse.californium.core.observe.ObserveRelation;
import org.eclipse.californium.core.observe.ObservingEndpoint;
import org.eclipse.californium.core.server.MessageDeliverer;
import org.eclipse.californium.core.server.resources.Resource;

public final class CustomMessageDeliverer implements MessageDeliverer, Observer {

	private static final int COAP_PORT = NetworkConfig.getStandard().getInt(
			NetworkConfig.Keys.COAP_PORT);

	private static final String CONFIG_FILE_LOCATION="config/server_config.xml";
	CoapServer server;
	private IObservable observable = null;
	Exchange exchange;
	Resource resource;
	Request request;
	int observeOption = 12;

	public CustomMessageDeliverer(IObservable ov) {
		this.observable = ov;
	}

	public CustomMessageDeliverer(CoapServer server) {
		this.server = server;
	}

	public synchronized void deliverRequest(final Exchange exchange) {
		// TODO Auto-generated method stub
		this.exchange = exchange;
		request = exchange.getRequest();
		resource = createResource(exchange);
		
		XMLParserConfig xmlParserConfig = new XMLParserConfig();
		ServerNodeInfo nodeInfo = xmlParserConfig.parseResources(
				CONFIG_FILE_LOCATION, request.getOptions()
						.getUriPathString());
		
		System.out.println(exchange.getRequest().toString());
		if (request.getCode() == Code.GET) {
			if (request.getOptions().hasObserve()) {
				InetAddress stAddr = null;

				try {
					stAddr = getLocalAddress();//InetAddress.getByName("10.102.201.93");
				} catch (SocketException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				InetSocketAddress bindToAddress = new InetSocketAddress(stAddr,
						COAP_PORT);
				// wait for user
				ObservingEndpoint remote = new ObservingEndpoint(bindToAddress);
				ObserveRelation obRelation = new ObserveRelation(remote,
						resource, exchange);
				remote.addObserveRelation(obRelation);
				exchange.setRelation(obRelation);

				resource.handleRequest(exchange);
				// respondOBSERVERequest(exchange);

			} else {
				try {
					new ClientExampleRunner(new ReadExample(exchange,
							nodeInfo.getOPCUANodeIdentifier())).run();
				} catch (ExecutionException | InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				resource.handleRequest(this.exchange);
			}
		}
		if (request.getCode() == Code.POST)
		{
			try {
				if(isValueInteger(request.getPayloadString()))
				{
					int payload = Integer.parseInt(request.getPayloadString());
					new ClientExampleRunner(new WriteExample(exchange,
							nodeInfo.getOPCUANodeIdentifier(), payload)).run();
					resource.handleRequest(this.exchange);
				}
				else
				{
					Response response = new Response(ResponseCode.BAD_REQUEST);

					response.setPayload("Wrong data type!");
					exchange.sendResponse(response);
				}
			} catch (ExecutionException | InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// deliverResponse(exchange, response);
		System.out.println(exchange.getRequest().toString());
	}

	public void deliverResponse(Exchange exchange, Response response) {
		// TODO Auto-generated method stub

	}

	public void respondGETRequest(Exchange exchange, Object value) {

		Response response = new Response(ResponseCode.CONTENT);
		response.setPayload(value.toString());
		exchange.sendResponse(response);
	}

	public void respondOBSERVERequest(Exchange exchange, String value) {
		Response response = new Response(ResponseCode.CHANGED);

		response.setPayload(value);
		exchange.sendResponse(response);

	}

	public Resource createResource(Exchange exchange) {
		DynamicCoAPResource resource = new DynamicCoAPResource(exchange
				.getCurrentRequest().getOptions().getUriPathString());
		server.add(resource);
		return resource;
	}

	@Override
	public synchronized void update(Observable updateObservable, Object arg) {
		// TODO Auto-generated method stub
		if (updateObservable == observable & observable.getExchange().getRequest().getCode()==Code.GET) {
			System.out.println("MQTT Client Observer : " + observable.getValue());
			respondGETRequest(observable.getExchange(), observable.getValue());
		}
	}
	
	public static boolean isValueInteger(String s) {
	    try { 
	        Integer.parseInt(s); 
	    } catch(NumberFormatException e) { 
	        return false; 
	    }
	    return true;
	}
	
	public static InetAddress getLocalAddress() throws SocketException
	  {
	    Enumeration<NetworkInterface> ifaces = NetworkInterface.getNetworkInterfaces();
	    while( ifaces.hasMoreElements() )
	    {
	      NetworkInterface iface = ifaces.nextElement();
	      Enumeration<InetAddress> addresses = iface.getInetAddresses();

	      while( addresses.hasMoreElements() )
	      {
	        InetAddress addr = addresses.nextElement();
	        if( addr instanceof Inet4Address && !addr.isLoopbackAddress() )
	        {
	          return addr;
	        }
	      }
	    }

	    return null;
	  }
}
