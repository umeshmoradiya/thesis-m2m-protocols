package iwu.thesis.modules.thesis_coap_opcua.opcua;

import iwu.thesis.modules.thesis_coap_opcua.CustomMessageDeliverer;
import iwu.thesis.modules.thesis_coap_opcua.IObservable;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.eclipse.californium.core.network.Exchange;
import org.eclipse.milo.opcua.sdk.client.OpcUaClient;
import org.eclipse.milo.opcua.stack.core.types.builtin.DataValue;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;
import org.eclipse.milo.opcua.stack.core.types.builtin.StatusCode;
import org.eclipse.milo.opcua.stack.core.types.builtin.Variant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableList;

public class WriteExample implements ClientExample {

	String nodePathString;
	Exchange ex;
	Object writeValue;

	/*
	 * public static void main(String[] args) throws Exception { WriteExample
	 * example = new WriteExample();
	 * 
	 * new ClientExampleRunner(example).run(); }
	 */

	public WriteExample(Exchange exchange, String nodePath, Object value)
			throws ExecutionException, InterruptedException {
		// TODO Auto-generated constructor stub
		this.ex = exchange;
		this.nodePathString = nodePath;
		this.writeValue = value;
		// this.example = new ReadExample();
		// new ClientExampleRunner(this.example).run();
	}

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public void run(OpcUaClient client, CompletableFuture<OpcUaClient> future)
			throws Exception {
		// synchronous connect
		client.connect().get();

		List<NodeId> nodeIds = ImmutableList.of(new NodeId(2, nodePathString));

		// for (int i = 0; i < 10; i++) {
		Variant v = new Variant(writeValue);

		// don't write status or timestamps
		DataValue dv = new DataValue(v, null, null);

		// write asynchronously....
		CompletableFuture<List<StatusCode>> f = client.writeValues(nodeIds,
				ImmutableList.of(dv));

		// ...but block for the results so we write in order
		List<StatusCode> statusCodes = f.get();
		StatusCode status = statusCodes.get(0);

		if (status.isGood()) {
			logger.info("Wrote '{}' to nodeId={}", v, nodeIds.get(0));
			IObservable ov = new IObservable(writeValue);
			CustomMessageDeliverer to = new CustomMessageDeliverer(ov);
			ov.addObserver(to);
			ov.setExchange(ex);
			ov.setValue(writeValue);
		}
		// }

		future.complete(client);
	}

}