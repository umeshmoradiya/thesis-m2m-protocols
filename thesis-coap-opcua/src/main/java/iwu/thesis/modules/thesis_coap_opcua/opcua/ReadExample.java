package iwu.thesis.modules.thesis_coap_opcua.opcua;

import iwu.thesis.modules.thesis_coap_opcua.CustomMessageDeliverer;
import iwu.thesis.modules.thesis_coap_opcua.IObservable;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.eclipse.californium.core.network.Exchange;
import org.eclipse.milo.opcua.sdk.client.OpcUaClient;
import org.eclipse.milo.opcua.sdk.client.api.nodes.attached.UaVariableNode;
import org.eclipse.milo.opcua.stack.core.Identifiers;
import org.eclipse.milo.opcua.stack.core.types.builtin.DataValue;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;
import org.eclipse.milo.opcua.stack.core.types.enumerated.ServerState;
import org.eclipse.milo.opcua.stack.core.types.enumerated.TimestampsToReturn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableList;

public class ReadExample implements ClientExample {

	String nodePathString;
	Exchange ex;
	ReadExample example;

	public static void main(String[] args) throws Exception {
		ReadExample example = new ReadExample();

		new ClientExampleRunner(example).run();
	}

	public ReadExample(Exchange exchange, String nodePath)
			throws ExecutionException, InterruptedException {
		// TODO Auto-generated constructor stub
		this.ex = exchange;
		this.nodePathString = nodePath;
		// this.example = new ReadExample();
		// new ClientExampleRunner(this.example).run();
	}

	public ReadExample() {
	}

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public void run(OpcUaClient client, CompletableFuture<OpcUaClient> future)
			throws Exception {
		// synchronous connect
		client.connect().get();

		System.out.println("Read path" + nodePathString);

		// synchronous read request via UaVariableNode
		UaVariableNode node = client.getAddressSpace().getVariableNode(
				new NodeId(2, nodePathString));
		DataValue value = node.readValue().get();

		logger.info("StartTime={}", value.getValue().getValue());
		System.out.println("Read Value" + value.getValue().getValue());
		IObservable ov = new IObservable(value.getValue().getValue().toString());
		CustomMessageDeliverer to = new CustomMessageDeliverer(ov);
		ov.addObserver(to);
		ov.setExchange(ex);
		ov.setValue(value.getValue().getValue());
		// asynchronous read request
		/*readServerStateAndTime(client).thenAccept(
				values -> {
					DataValue v0 = values.get(0);
					DataValue v1 = values.get(1);

					logger.info("State={}", ServerState.from((Integer) v0
							.getValue().getValue()));
					logger.info("CurrentTime={}", v1.getValue().getValue());

					future.complete(client);
				});*/
		future.complete(client);
	}

	private CompletableFuture<List<DataValue>> readServerStateAndTime(
			OpcUaClient client) {
		List<NodeId> nodeIds = ImmutableList.of(
				Identifiers.Server_ServerStatus_State,
				Identifiers.Server_ServerStatus_CurrentTime);

		return client.readValues(0.0, TimestampsToReturn.Both, nodeIds);
	}

}
