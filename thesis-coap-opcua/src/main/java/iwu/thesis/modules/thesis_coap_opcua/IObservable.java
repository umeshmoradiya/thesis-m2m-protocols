package iwu.thesis.modules.thesis_coap_opcua;

import java.util.Observable;

import org.eclipse.californium.core.network.Exchange;

public class IObservable extends Observable {
	private Object n = "obs";
	private Exchange ex;

	public IObservable(Object n) {
		this.n = n;
	}

	public void setValue(Object n) {
		this.n = n;
		setChanged();
		notifyObservers();
	}

	public Object getValue() {
		return n;
	}

	public Exchange getExchange() {
		return ex;
	}

	public void setExchange(Exchange exchange) {
		this.ex = exchange;
	}

}
