package iwu.thesis.modules.thesis_coap_opcua;

import org.eclipse.californium.core.coap.CoAP;
import org.eclipse.californium.core.coap.CoAP.Type;
import org.eclipse.californium.core.server.resources.CoapExchange;
import org.eclipse.californium.core.server.resources.ConcurrentCoapResource;

public class DynamicCoAPResource extends ConcurrentCoapResource {

	public DynamicCoAPResource(String resource) {

		// set resource identifier
		super(resource);
		setObservable(true);
		setObserveType(Type.CON);
		getAttributes().setTitle("resource");

	}

	@Override
	public void handleGET(CoapExchange exchange) {
		//System.out.println("DynamicROLLEXResource handleCoAPGET() : " + buffer);
		//exchange.respond("DynamichandleCoAPGET() : " + buffer);

	}

	@Override
	public void handlePOST(CoapExchange exchange) {
		// TODO Auto-generated method stub
		System.out.println("DynamicCoAPResource handlePOST() : "
				+ exchange.getRequestText());
		//exchange.respond("DynamichandlePOST() : " + exchange.getRequestText());
		exchange.respond(CoAP.ResponseCode.CONTENT);
		changed();
	}

}
