package iwu.thesis.modules.thesis_opcua_coap;

import static org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.Unsigned.ubyte;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import org.eclipse.milo.opcua.sdk.core.AccessLevel;
import org.eclipse.milo.opcua.sdk.core.Reference;
import org.eclipse.milo.opcua.sdk.server.OpcUaServer;
import org.eclipse.milo.opcua.sdk.server.api.DataItem;
import org.eclipse.milo.opcua.sdk.server.api.MethodInvocationHandler;
import org.eclipse.milo.opcua.sdk.server.api.MonitoredItem;
import org.eclipse.milo.opcua.sdk.server.api.Namespace;
import org.eclipse.milo.opcua.sdk.server.model.UaFolderNode;
import org.eclipse.milo.opcua.sdk.server.model.UaMethodNode;
import org.eclipse.milo.opcua.sdk.server.model.UaNode;
import org.eclipse.milo.opcua.sdk.server.model.UaVariableNode;
import org.eclipse.milo.opcua.sdk.server.util.AnnotationBasedInvocationHandler;
import org.eclipse.milo.opcua.sdk.server.util.SubscriptionModel;
import org.eclipse.milo.opcua.stack.core.AttributeId;
import org.eclipse.milo.opcua.stack.core.Identifiers;
import org.eclipse.milo.opcua.stack.core.StatusCodes;
import org.eclipse.milo.opcua.stack.core.UaException;
import org.eclipse.milo.opcua.stack.core.types.builtin.DataValue;
import org.eclipse.milo.opcua.stack.core.types.builtin.LocalizedText;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;
import org.eclipse.milo.opcua.stack.core.types.builtin.QualifiedName;
import org.eclipse.milo.opcua.stack.core.types.builtin.StatusCode;
import org.eclipse.milo.opcua.stack.core.types.builtin.Variant;
import org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.UShort;
import org.eclipse.milo.opcua.stack.core.types.enumerated.NodeClass;
import org.eclipse.milo.opcua.stack.core.types.enumerated.TimestampsToReturn;
import org.eclipse.milo.opcua.stack.core.types.structured.ReadValueId;
import org.eclipse.milo.opcua.stack.core.types.structured.WriteValue;
import org.eclipse.milo.opcua.stack.core.util.FutureUtils;
import org.json.XML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;

public class HelloNamespace implements Namespace {

	public static final String NAMESPACE_URI = "urn:eclipse:milo:hello-world";

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final SubscriptionModel subscriptionModel;

	private final OpcUaServer server;
	private final UShort namespaceIndex;
	
	private static final String CONFIG_FILE_LOCATION = "config/server_config.xml";

	public HelloNamespace(OpcUaServer server, UShort namespaceIndex) {
		this.server = server;
		this.namespaceIndex = namespaceIndex;

		subscriptionModel = new SubscriptionModel(server, this);

		try {
			// Create a "HelloWorld" folder and add it to the node manager
			NodeId folderNodeId = new NodeId(namespaceIndex, "HelloWorld");

			UaFolderNode folderNode = new UaFolderNode(server.getNodeManager(),
					folderNodeId, new QualifiedName(namespaceIndex,
							"HelloWorld"), LocalizedText.english("HelloWorld"));

			server.getNodeManager().addNode(folderNode);

			// Make sure our new folder shows up under the server's Objects
			// folder
			server.getUaNamespace().addReference(Identifiers.ObjectsFolder,
					Identifiers.Organizes, true, folderNodeId.expanded(),
					NodeClass.Object);
			
			// Add the rest of the nodes
			getNodes();
			addNodes(folderNode);

			addMethodNode(folderNode);
		} catch (UaException e) {
			logger.error("Error adding nodes: {}", e.getMessage(), e);
		}
	}

	@Override
	public UShort getNamespaceIndex() {
		return namespaceIndex;
	}

	@Override
	public String getNamespaceUri() {
		return NAMESPACE_URI;
	}

	private static final Object[][] STATIC_SCALAR_NODES = new Object[][] {
			{ "Double", Identifiers.Double, new Variant(3.14d),
					"iwu/rollex/powermeter" },
			{ "Float", Identifiers.Float, new Variant(3.14f),
					"iwu/rollex/temperature" },
			{ "Int32", Identifiers.Int32, new Variant(32), "iwu/fofab/nshv" },
			{ "String", Identifiers.String, new Variant("string value"),
					"iwu/fofab/solarplant" }, };
private Object[][] getNodes(){
	Object[][] STATIC_SCALAR_NODES = null;
	XMLParserConfig x = new XMLParserConfig();
	
	return STATIC_SCALAR_NODES;
}
	private void addNodes(UaFolderNode folderNode) {
		for (Object[] os : STATIC_SCALAR_NODES) {
			String name = (String) os[0];
			NodeId typeId = (NodeId) os[1];
			Variant variant = (Variant) os[2];
			String identifier = (String) os[3];

			UaVariableNode node = addNewUaVariableNode(name, typeId, variant,
					identifier);

			folderNode.addOrganizes(node);
		}

	}

	private UaVariableNode addNewUaVariableNode(String name, NodeId typeId,
			Variant variant, String identifier) {
		UaVariableNode node = new UaVariableNode.UaVariableNodeBuilder(
				server.getNodeManager())
				.setNodeId(new NodeId(namespaceIndex, identifier))
				.setAccessLevel(
						ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
				.setBrowseName(new QualifiedName(namespaceIndex, name))
				.setDisplayName(LocalizedText.english(name))
				.setDataType(typeId)
				.setTypeDefinition(Identifiers.BaseDataVariableType).build();

		node.setValue(new DataValue(variant));

		server.getNodeManager().addNode(node);

		return node;
	}

	private void addMethodNode(UaFolderNode folderNode) {
		UaMethodNode methodNode = UaMethodNode
				.builder(server.getNodeManager())
				.setNodeId(new NodeId(namespaceIndex, "HelloWorld/sqrt(x)"))
				.setBrowseName(new QualifiedName(namespaceIndex, "sqrt(x)"))
				.setDisplayName(new LocalizedText(null, "sqrt(x)"))
				.setDescription(
						LocalizedText
								.english("Returns the correctly rounded positive square root of a double value."))
				.build();

		try {
			AnnotationBasedInvocationHandler invocationHandler = AnnotationBasedInvocationHandler
					.fromAnnotatedObject(server.getNodeManager(),
							new SqrtMethod());

			methodNode.setProperty(UaMethodNode.InputArguments,
					invocationHandler.getInputArguments());
			methodNode.setProperty(UaMethodNode.OutputArguments,
					invocationHandler.getOutputArguments());
			methodNode.setInvocationHandler(invocationHandler);

			server.getNodeManager().addNode(methodNode);

			folderNode.addReference(new Reference(folderNode.getNodeId(),
					Identifiers.HasComponent,
					methodNode.getNodeId().expanded(), methodNode
							.getNodeClass(), true));
		} catch (Exception e) {
			logger.error("Error creating sqrt() method.", e);
		}
	}

	@Override
	public CompletableFuture<List<Reference>> getReferences(NodeId nodeId) {
		UaNode node = server.getNodeManager().get(nodeId);

		if (node != null) {
			return CompletableFuture.completedFuture(node.getReferences());
		} else {
			return FutureUtils.failedFuture(new UaException(
					StatusCodes.Bad_NodeIdUnknown));
		}
	}

	@Override
	public void read(ReadContext context, Double maxAge,
			TimestampsToReturn timestamps, List<ReadValueId> readValueIds) {

		List<DataValue> results = Lists.newArrayListWithCapacity(readValueIds
				.size());

		for (ReadValueId readValueId : readValueIds) {
			UaNode node = server.getNodeManager().get(readValueId.getNodeId());

			if (node != null) {
				DataValue value = node.readAttribute(readValueId
						.getAttributeId().intValue(), timestamps, readValueId
						.getIndexRange());
				System.out.println(readValueId.getNodeId().getIdentifier());
				XMLParserConfig xmlParserConfig = new XMLParserConfig();
				ServerNodeInfo nodeInfo = xmlParserConfig.parseResources(
						CONFIG_FILE_LOCATION, readValueId.getNodeId()
								.getIdentifier().toString());

				ClientCoAP clientCoAP = new ClientCoAP();
				// clientCoAP.sendCoAPGET(nodeInfo.getAddress());
				DataValue dataValue = new DataValue(new Variant(
						clientCoAP.sendCoAPGET(nodeInfo.getAddress())));
				results.add(dataValue);
			} else {
				results.add(new DataValue(StatusCodes.Bad_NodeIdUnknown));
			}
		}

		context.complete(results);
	}

	@Override
	public void write(WriteContext context, List<WriteValue> writeValues) {
		List<StatusCode> results = Lists.newArrayListWithCapacity(writeValues
				.size());

		System.out.println("OPC UA Write CoAP");
		for (WriteValue writeValue : writeValues) {
			UaNode node = server.getNodeManager().get(writeValue.getNodeId());

			if (node != null) {
				try {
					node.writeAttribute(server.getNamespaceManager(),
							writeValue.getAttributeId(), writeValue.getValue(),
							writeValue.getIndexRange());

					XMLParserConfig xmlParserConfig = new XMLParserConfig();
					ServerNodeInfo nodeInfo = xmlParserConfig.parseResources(
							CONFIG_FILE_LOCATION, writeValue.getNodeId()
									.getIdentifier().toString());
					System.out.println(writeValue.getNodeId().getIdentifier());
					System.out.println(nodeInfo.getAddress());
					ClientCoAP clientCoAP = new ClientCoAP();
					clientCoAP.sendCoAPPOST(nodeInfo.getAddress(), writeValue
							.getValue().getValue().getValue().toString());

					results.add(StatusCode.GOOD);
					System.out.println("Write OPC");
					logger.info("Wrote value {} to {} attribute of {}",
							writeValue.getValue().getValue(),
							AttributeId.from(writeValue.getAttributeId()),
							node.getNodeId());
				} catch (UaException e) {
					logger.error("Unable to write value={}",
							writeValue.getValue(), e);
					results.add(e.getStatusCode());
				}
			} else {

				results.add(new StatusCode(StatusCodes.Bad_NodeIdUnknown));
			}
		}
		context.complete(results);
	}

	@Override
	public void onDataItemsCreated(List<DataItem> dataItems) {
		subscriptionModel.onDataItemsCreated(dataItems);
	}

	@Override
	public void onDataItemsModified(List<DataItem> dataItems) {
		subscriptionModel.onDataItemsModified(dataItems);
	}

	@Override
	public void onDataItemsDeleted(List<DataItem> dataItems) {
		subscriptionModel.onDataItemsDeleted(dataItems);
	}

	@Override
	public void onMonitoringModeChanged(List<MonitoredItem> monitoredItems) {
		subscriptionModel.onMonitoringModeChanged(monitoredItems);
	}

	@Override
	public Optional<MethodInvocationHandler> getInvocationHandler(
			NodeId methodId) {
		Optional<UaNode> node = server.getNodeManager().getNode(methodId);

		return node.flatMap(n -> {
			if (n instanceof UaMethodNode) {
				return ((UaMethodNode) n).getInvocationHandler();
			} else {
				return Optional.empty();
			}
		});
	}

}