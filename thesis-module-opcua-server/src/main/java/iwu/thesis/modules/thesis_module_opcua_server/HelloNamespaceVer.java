package iwu.thesis.modules.thesis_module_opcua_server;

import static org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.Unsigned.ubyte;
import static org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.Unsigned.uint;
import static org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.Unsigned.ulong;
import static org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.Unsigned.ushort;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import org.eclipse.milo.opcua.sdk.core.AccessLevel;
import org.eclipse.milo.opcua.sdk.core.Reference;
import org.eclipse.milo.opcua.sdk.server.OpcUaServer;
import org.eclipse.milo.opcua.sdk.server.api.DataItem;
import org.eclipse.milo.opcua.sdk.server.api.MonitoredItem;
import org.eclipse.milo.opcua.sdk.server.api.Namespace;
import org.eclipse.milo.opcua.sdk.server.model.UaFolderNode;
import org.eclipse.milo.opcua.sdk.server.model.UaNode;
import org.eclipse.milo.opcua.sdk.server.model.UaVariableNode;
import org.eclipse.milo.opcua.sdk.server.util.SubscriptionModel;
import org.eclipse.milo.opcua.stack.core.Identifiers;
import org.eclipse.milo.opcua.stack.core.StatusCodes;
import org.eclipse.milo.opcua.stack.core.UaException;
import org.eclipse.milo.opcua.stack.core.types.builtin.ByteString;
import org.eclipse.milo.opcua.stack.core.types.builtin.DataValue;
import org.eclipse.milo.opcua.stack.core.types.builtin.DateTime;
import org.eclipse.milo.opcua.stack.core.types.builtin.LocalizedText;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;
import org.eclipse.milo.opcua.stack.core.types.builtin.QualifiedName;
import org.eclipse.milo.opcua.stack.core.types.builtin.StatusCode;
import org.eclipse.milo.opcua.stack.core.types.builtin.Variant;
import org.eclipse.milo.opcua.stack.core.types.builtin.XmlElement;
import org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.UShort;
import org.eclipse.milo.opcua.stack.core.types.enumerated.NodeClass;
import org.eclipse.milo.opcua.stack.core.types.enumerated.TimestampsToReturn;
import org.eclipse.milo.opcua.stack.core.types.structured.ReadValueId;
import org.eclipse.milo.opcua.stack.core.types.structured.WriteValue;
import org.eclipse.milo.opcua.stack.core.util.FutureUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;

public class HelloNamespaceVer implements Namespace {

	public static final String NAMESPACE_URI = "urn:eclipse:milo:opcua:test-namespace";

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final SubscriptionModel subscriptionModel;

	private final OpcUaServer server;
	private final UShort namespaceIndex;

	public HelloNamespaceVer(OpcUaServer server, UShort namespaceIndex) {
		this.server = server;
		this.namespaceIndex = namespaceIndex;

		subscriptionModel = new SubscriptionModel(server, this);

		try {
			// Create a "HelloWorld" folder and add it to the node manager
			NodeId folderNodeId = new NodeId(namespaceIndex, "HelloWorld");

			UaFolderNode folderNode = new UaFolderNode(server.getNodeManager(),
					folderNodeId, new QualifiedName(namespaceIndex,
							"HelloWorld"), LocalizedText.english("HelloWorld"));

			server.getNodeManager().addNode(folderNode);

			// Make sure our new folder shows up under the server's Objects
			// folder
			server.getUaNamespace().addReference(Identifiers.ObjectsFolder,
					Identifiers.Organizes, true, folderNodeId.expanded(),
					NodeClass.Object);

			// Add the rest of the nodes
			addNodes(folderNode);
		} catch (UaException e) {
			logger.error("Error adding nodes: {}", e.getMessage(), e);
		}
	}

	@Override
	public UShort getNamespaceIndex() {
		return namespaceIndex;
	}

	@Override
	public String getNamespaceUri() {
		return NAMESPACE_URI;
	}

	private static final Object[][] STATIC_SCALAR_NODES = new Object[][] {
			{ "Bool", Identifiers.Boolean, new Variant(false) },
			{ "Byte", Identifiers.Byte, new Variant(ubyte(0x00)) },
			{
					"ByteString",
					Identifiers.ByteString,
					new Variant(new ByteString(new byte[] { 0x01, 0x02, 0x03,
							0x04 })) },
			{ "DateTime", Identifiers.DateTime, new Variant(DateTime.now()) },
			{ "Double", Identifiers.Double, new Variant(3.14d) },
			{ "Duration", Identifiers.Duration, new Variant(1.0) },
			{ "Float", Identifiers.Float, new Variant(3.14f) },
			{ "Guid", Identifiers.Guid, new Variant(UUID.randomUUID()) },
			{ "Int16", Identifiers.Int16, new Variant((short) 16) },
			{ "Int32", Identifiers.Int32, new Variant(32) },
			{ "Int64", Identifiers.Int64, new Variant(64L) },
			{ "LocalizedText", Identifiers.LocalizedText,
					new Variant(LocalizedText.english("localized text")) },
			{ "NodeId", Identifiers.NodeId,
					new Variant(new NodeId(1234, "abcd")) },
			{ "QualifiedName", Identifiers.QualifiedName,
					new Variant(new QualifiedName(1234, "defg")) },
			{ "SByte", Identifiers.SByte, new Variant((byte) 0x00) },
			{ "String", Identifiers.String, new Variant("string value") },
			{ "UtcTime", Identifiers.UtcTime, new Variant(DateTime.now()) },
			{ "UInt16", Identifiers.UInt16, new Variant(ushort(16)) },
			{ "UInt32", Identifiers.UInt32, new Variant(uint(32)) },
			{ "UInt64", Identifiers.UInt64, new Variant(ulong(64L)) },
			{ "XmlElement", Identifiers.XmlElement,
					new Variant(new XmlElement("<a>hello</a>")) }, };

	private void addNodes(UaFolderNode folderNode) {
		for (Object[] os : STATIC_SCALAR_NODES) {
			String name = (String) os[0];
			NodeId typeId = (NodeId) os[1];
			Variant variant = (Variant) os[2];

			UaVariableNode node = new UaVariableNode.UaVariableNodeBuilder(
					server.getNodeManager())
					.setNodeId(new NodeId(namespaceIndex, "HelloWorld/" + name))
					.setAccessLevel(
							ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
					.setBrowseName(new QualifiedName(namespaceIndex, name))
					.setDisplayName(LocalizedText.english(name))
					.setDataType(typeId)
					.setTypeDefinition(Identifiers.BaseDataVariableType)
					.build();

			node.setValue(new DataValue(variant));

			server.getNodeManager().addNode(node);

			folderNode.addOrganizes(node);
		}
	}

	@Override
	public CompletableFuture<List<Reference>> getReferences(NodeId nodeId) {
		UaNode node = server.getNodeManager().get(nodeId);

		if (node != null) {
			return CompletableFuture.completedFuture(node.getReferences());
		} else {
			return FutureUtils.failedFuture(new UaException(
					StatusCodes.Bad_NodeIdUnknown));
		}
	}

	@Override
	public void read(ReadContext context, Double maxAge,
			TimestampsToReturn timestamps, List<ReadValueId> readValueIds) {

		List<DataValue> results = Lists.newArrayListWithCapacity(readValueIds
				.size());

		for (ReadValueId readValueId : readValueIds) {
			UaNode node = server.getNodeManager().get(readValueId.getNodeId());

			if (node != null) {
				System.out.println("hello OPC READ: \t "
						+ node.getNodeId().getIdentifier());

				DataValue value = node.readAttribute(readValueId
						.getAttributeId().intValue(), timestamps, readValueId
						.getIndexRange());

				results.add(value);
				System.out.println("hello OPC READ VAlue: \t " + value);
			} else {
				results.add(new DataValue(StatusCodes.Bad_NodeIdUnknown));
			}
		}

		context.complete(results);
	}

	@Override
	public void write(WriteContext context, List<WriteValue> writeValues) {
		List<StatusCode> results = Lists.newArrayListWithCapacity(writeValues
				.size());

		for (WriteValue writeValue : writeValues) {
			UaNode node = server.getNodeManager().get(writeValue.getNodeId());

			if (node != null) {
				try {
					System.out.println("hello OPC Write: \t "
							+ node.getNodeId().getIdentifier());
					// System.out.println("hello OPC Write Value: \t " +
					// writeValue.getValue());
					node.writeAttribute(server.getNamespaceManager(),
							writeValue.getAttributeId(), writeValue.getValue(),
							writeValue.getIndexRange());

					results.add(StatusCode.GOOD);
				} catch (UaException e) {
					logger.error("Unable to write value={}",
							writeValue.getValue(), e);
					results.add(e.getStatusCode());
				}
			} else {
				results.add(new StatusCode(StatusCodes.Bad_NodeIdUnknown));
			}
		}

		context.complete(results);
	}

	@Override
	public void onDataItemsCreated(List<DataItem> dataItems) {
		subscriptionModel.onDataItemsCreated(dataItems);
	}

	@Override
	public void onDataItemsModified(List<DataItem> dataItems) {
		subscriptionModel.onDataItemsModified(dataItems);
	}

	@Override
	public void onDataItemsDeleted(List<DataItem> dataItems) {
		subscriptionModel.onDataItemsDeleted(dataItems);
	}

	@Override
	public void onMonitoringModeChanged(List<MonitoredItem> monitoredItems) {
		subscriptionModel.onMonitoringModeChanged(monitoredItems);
	}

}