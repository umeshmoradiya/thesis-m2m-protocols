package iwu.thesis.modules.thesis_module_opcua_server;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.io.Files.createTempDir;
import static java.util.Collections.singletonList;
import static org.eclipse.milo.opcua.sdk.server.api.config.OpcUaServerConfig.USER_TOKEN_POLICY_ANONYMOUS;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.eclipse.milo.opcua.sdk.server.OpcUaServer;
import org.eclipse.milo.opcua.sdk.server.api.config.OpcUaServerConfig;
import org.eclipse.milo.opcua.stack.core.application.DefaultCertificateManager;
import org.eclipse.milo.opcua.stack.core.application.DefaultCertificateValidator;

public class ServerOPC {

	public static void main(String[] args) throws ExecutionException,
			InterruptedException {
		ServerOPC server = new ServerOPC();

		server.startup().get();

		CompletableFuture<Void> future = new CompletableFuture<>();

		Runtime.getRuntime().addShutdownHook(
				new Thread(() -> future.complete(null)));

		future.get();
	}

	private final OpcUaServer server;

	public ServerOPC() throws ExecutionException, InterruptedException {
		OpcUaServerConfig serverConfig = OpcUaServerConfig
				.builder()
				.setApplicationUri("urn:eclipse:milo:examples:server")
				.setBindAddresses(newArrayList("localhost"))
				.setBindPort(4840)
				.setCertificateManager(new DefaultCertificateManager())
				.setCertificateValidator(
						new DefaultCertificateValidator(createTempDir()))
				.setServerName("example")
				.setUserTokenPolicies(
						singletonList(USER_TOKEN_POLICY_ANONYMOUS)).build();

		server = new OpcUaServer(serverConfig);

		server.getNamespaceManager().registerAndAdd(
				HelloNamespace.NAMESPACE_URI,
				idx -> new HelloNamespace(server, idx));
		System.out.println("Server Started");
	}

	public OpcUaServer getServer() {
		return server;
	}

	public CompletableFuture<OpcUaServer> startup() {
		return server.startup();
	}

	public CompletableFuture<OpcUaServer> shutdown() {
		return server.shutdown();
	}

}