package iwu.thesis.modules.thesis_module_opcua_server;

/**
 * Hello world!
 *
 */
public class AppOPCUAServer {
	public static void main(String[] args) throws Exception {
		AppOPCUAServer appServer = new AppOPCUAServer();

		appServer.startOPCServer();
	}

	protected void startOPCServer() throws Exception {
		System.out.println("Starting OPC Server...");
		ServerOPC serverOPC = new ServerOPC();
		// serverOPC.createServer();
		System.out.println("Started OPC Server...");
	}
}
