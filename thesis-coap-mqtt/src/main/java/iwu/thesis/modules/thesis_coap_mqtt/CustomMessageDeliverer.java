package iwu.thesis.modules.thesis_coap_mqtt;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.Observable;
import java.util.Observer;

import org.eclipse.californium.core.CoapServer;
import org.eclipse.californium.core.coap.CoAP.Code;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.coap.OptionSet;
import org.eclipse.californium.core.coap.Request;
import org.eclipse.californium.core.coap.Response;
import org.eclipse.californium.core.network.Exchange;
import org.eclipse.californium.core.network.config.NetworkConfig;
import org.eclipse.californium.core.observe.ObserveRelation;
import org.eclipse.californium.core.observe.ObservingEndpoint;
import org.eclipse.californium.core.server.MessageDeliverer;
import org.eclipse.californium.core.server.resources.Resource;
import org.eclipse.paho.client.mqttv3.MqttException;

public final class CustomMessageDeliverer implements MessageDeliverer, Observer {
	
	private static final int COAP_PORT = NetworkConfig.getStandard().getInt(
			NetworkConfig.Keys.COAP_PORT);

	private static final String CONFIG_FILE_LOCATION="config/server_config.xml";
	CoapServer server;
	private IObservable observable = null;
	Exchange exchange;
	Resource resource;
	Request request;
	int observeOption = 12;
	
	public static final String PREFIX_OBSERVE = "/O";
	public static final String PREFIX_GET = "/R";
	public static final String PREFIX_POST = "/W";
	
	public CustomMessageDeliverer(IObservable observable) {
		this.observable = observable;
	}

	public CustomMessageDeliverer(CoapServer server) {
		this.server = server;
	}

	public synchronized void deliverRequest(final Exchange exchange) {
		// TODO Auto-generated method stub
		this.exchange = exchange;
		request = this.exchange.getRequest();
		resource = createResource(this.exchange);
		
		XMLParserConfig xmlParseConfing = new XMLParserConfig();
		ServerNodeInfo nodeInfo = xmlParseConfing.parseResources(
				CONFIG_FILE_LOCATION, request.getOptions()
						.getUriPathString());
		
		System.out.println(this.exchange.getRequest().toString());
		if (request.getCode() == Code.GET) {
			if (request.getOptions().hasObserve()) {
				InetAddress localInetAddress = null;
				try {
					localInetAddress = getLocalAddress();//InetAddress.getByName("192.168.0.109");
				} catch (SocketException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				InetSocketAddress bindToAddress = new InetSocketAddress(localInetAddress,
						COAP_PORT);
				// wait for user
				ObservingEndpoint remote = new ObservingEndpoint(bindToAddress);
				ObserveRelation obRelation = new ObserveRelation(remote,
						resource, this.exchange);
				remote.addObserveRelation(obRelation);
				exchange.setRelation(obRelation);
				
				ClientMQTT clientMQTT = new ClientMQTT(nodeInfo.getAddress(),
						PREFIX_OBSERVE + nodeInfo.getMqttTopic());
				try {
					clientMQTT
							.subscribeTopic(PREFIX_OBSERVE
									+ nodeInfo.getMqttTopic(),
									this.exchange,true);

				} catch (MqttException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				resource.handleRequest(this.exchange);
				//respondOBSERVERequest(exchange);

			} else{
				//respondGETRequest(this.exchange);
				ClientMQTT clientMQTT = new ClientMQTT(nodeInfo.getAddress(),
						PREFIX_GET + nodeInfo.getMqttTopic());
				try {
					clientMQTT
							.subscribeTopic(PREFIX_GET
									+ nodeInfo.getMqttTopic(),
									this.exchange,false);

				} catch (MqttException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				resource.handleRequest(this.exchange);
			}
		}
		if (request.getCode() == Code.POST)
		{
			//respondPOSTRequest(this.exchange);
			ClientMQTT clientMQTT = new ClientMQTT(nodeInfo.getAddress(),
					PREFIX_POST + request.getOptions().getUriPathString());
			try {
				clientMQTT
						.publishTopic(PREFIX_POST
								+ nodeInfo.getMqttTopic(),
								request.getPayloadString());
				resource.handleRequest(this.exchange);
				//Response response = new Response(ResponseCode.CHANGED);
				//this.exchange.sendResponse(response);
			} catch (MqttException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// deliverResponse(exchange, response);
		System.out.println(exchange.getRequest().toString());
	}

	public void respondGETRequest(Exchange exchange, String value) {
		Response response = new Response(ResponseCode.CONTENT);
		response.setPayload(value);
		exchange.sendResponse(response);
	}

	public void respondOBSERVERequest(Exchange exchange, String value) {
		observeOption=observeOption+1;
		Response response = new Response(ResponseCode.CONTENT);
		response.setOptions(new OptionSet().setObserve(observeOption));
		response.setPayload(value);
		exchange.sendResponse(response);

	}

	public Resource createResource(Exchange exchange) {
		DynamicCoAPResource resource = new DynamicCoAPResource(exchange
				.getCurrentRequest().getOptions().getUriPathString());
		server.add(resource);
		return resource;
	}

	@Override
	public synchronized void update(Observable updateObservable, Object arg) {
		// TODO Auto-generated method stub
		if (updateObservable == observable) {
			if(observable.isObserve){
			System.out.println("MQTT Client Observer : " + observable.getValue());
			respondOBSERVERequest(observable.getExchange(), observable.getValue());
			}
			else{
				System.out.println("MQTT Client GET : " + observable.getValue());
				respondGETRequest(observable.getExchange(), observable.getValue());
			}
		}
	}
	
	public static InetAddress getLocalAddress() throws SocketException
	  {
	    Enumeration<NetworkInterface> ifaces = NetworkInterface.getNetworkInterfaces();
	    while( ifaces.hasMoreElements() )
	    {
	      NetworkInterface iface = ifaces.nextElement();
	      Enumeration<InetAddress> addresses = iface.getInetAddresses();

	      while( addresses.hasMoreElements() )
	      {
	        InetAddress addr = addresses.nextElement();
	        if( addr instanceof Inet4Address && !addr.isLoopbackAddress() )
	        {
	          return addr;
	        }
	      }
	    }

	    return null;
	  }

	@Override
	public void deliverResponse(Exchange exchange, Response response) {
		// TODO Auto-generated method stub
		
	}
}
