package iwu.thesis.modules.thesis_coap_mqtt;

import java.util.Observable;

import org.eclipse.californium.core.network.Exchange;

public class IObservable extends Observable {
	private String n = "obs";
	private Exchange coapExchange;
	boolean isObserve;

	public IObservable(String n) {
		this.n = n;
	}

	public void setValue(String n) {
		this.n = n;
		setChanged();
		notifyObservers();
	}

	public String getValue() {
		return n;
	}

	public Exchange getExchange() {
		return coapExchange;
	}

	public void setExchange(Exchange coapExchange) {
		this.coapExchange = coapExchange;
	}

	public boolean isObserve() {
		return isObserve;
	}

	public void setObserve(boolean isObserve) {
		this.isObserve = isObserve;
	}
	

}
