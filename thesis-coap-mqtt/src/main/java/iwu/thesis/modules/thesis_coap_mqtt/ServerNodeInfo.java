package iwu.thesis.modules.thesis_coap_mqtt;

public class ServerNodeInfo {

	private String protocol;
	private String address;
	private String name;
	private int id;
	private String mqttTopic;

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMqttTopic() {
		return mqttTopic;
	}

	public void setMqttTopic(String topic) {
		this.mqttTopic = topic;
	}

}
