package iwu.thesis.modules.thesis_coap_mqtt;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;

import org.eclipse.californium.core.CoapServer;
import org.eclipse.californium.core.network.CoapEndpoint;
import org.eclipse.californium.core.network.EndpointManager;
import org.eclipse.californium.core.network.config.NetworkConfig;

public class CoapAppServer extends CoapServer {

	private static final int COAP_PORT = NetworkConfig.getStandard().getInt(
			NetworkConfig.Keys.COAP_PORT); // 5683

	/**
	 * Constructor for a new CoapAppServer server. Here, the resources of the
	 * server are initialized.
	 */
	public CoapAppServer() throws SocketException {

		// provide an instance of a CustomMessageDeliverer resource
		setMessageDeliverer(new CustomMessageDeliverer(this));
	}
	
	/**
	 * Application entry point.
	 */
	public static void main(String[] args) {
		try {
			// create server
			CoapAppServer server = new CoapAppServer();

			// add endpoints on all IP addresses
			server.addEndpoints();
			server.start();

		} catch (SocketException e) {
			System.err
					.println("Failed to initialize server: " + e.getMessage());
		}
	}

	/**
	 * Add individual endpoints listening on default CoAP port on all IPv4
	 * addresses of all network interfaces.
	 */
	private void addEndpoints() {
		for (InetAddress addr : EndpointManager.getEndpointManager()
				.getNetworkInterfaces()) {
			// only binds to IPv4 addresses and localhost
			if (addr instanceof Inet4Address || addr.isLoopbackAddress()) {
				InetSocketAddress bindToAddress = new InetSocketAddress(addr,
						COAP_PORT);
				addEndpoint(new CoapEndpoint(bindToAddress));
			}
		}
	}
}
